Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2012, CS Systemes dInformation, France
 2011, 2012, Centre National dEtudes Spatiales (CNES), France
 2005, Herve Drolon, FreeImage Team
 2003-2014, Antonin Descampe
 2003-2009, Francois-Olivier Devaux
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002, 2003, Yannick Verschueren
 2001-2003, David Janssens
License: BSD-2-clause

Files: cmake/*
Copyright: no-info-found
License: BSD-2-clause

Files: cmake/FindCPPCHECK.cmake
 cmake/FindKAKADU.cmake
Copyright: 2006-2014, Mathieu Malaterre <mathieu.malaterre@gmail.com>
License: BSD-3-clause

Files: debian/*
Copyright: © 2014-2017, Mathieu Malaterre <malat@debian.org>
License: BSD-2

Files: doc/*
Copyright: 2011, Mickael Savinaud, Communications & Systemes <mickael.savinaud@c-s.fr>
 2002-2014, Universite catholique de Louvain (UCL), Belgium
License: BSD-2-clause

Files: doc/man/*
Copyright: 2006, 2007, Parvatha Elangovan
 2005, Herve Drolon, FreeImage Team
 2003-2007, Francois-Olivier Devaux and Antonin Descampe
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002, 2003, Yannick Verschueren
 2001-2003, David Janssens
License: BSD-2-clause

Files: doc/man/man1/opj_dump.1
Copyright: 2010, 2012, Mathieu Malaterre
License: BSD-2-clause

Files: doc/openjpip.dox.in
Copyright: 2010, 2011, Kaori Hagihara
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
License: BSD-2-clause

Files: src/*
Copyright: 2005, Herve Drolon, FreeImage Team
 2003-2014, Antonin Descampe
 2003-2007, Francois-Olivier Devaux
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002, 2003, Yannick Verschueren
 2001-2003, David Janssens
License: BSD-2-clause

Files: src/bin/common/opj_getopt.c
Copyright: 1987, 1993, 1994, The Regents of the University of California.
License: BSD-3-clause

Files: src/bin/common/opj_string.h
Copyright: 2015, Matthieu Darbois
License: BSD-2-clause

Files: src/bin/jp2/*
Copyright: 2006, 2007, Parvatha Elangovan
 2005, Herve Drolon, FreeImage Team
 2003-2014, Antonin Descampe
 2003-2007, Francois-Olivier Devaux
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002, 2003, Yannick Verschueren
 2001-2003, David Janssens
License: BSD-2-clause

Files: src/bin/jp2/convert.h
Copyright: 2005, Herve Drolon, FreeImage Team
 2003-2014, Antonin Descampe
 2003-2007, Francois-Olivier Devaux
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002, 2003, Yannick Verschueren
 2001-2003, David Janssens
License: BSD-2-clause

Files: src/bin/jp2/convertpng.c
 src/bin/jp2/converttif.c
Copyright: 2015, Matthieu Darbois
 2006, 2007, Parvatha Elangovan
 2005, Herve Drolon, FreeImage Team
 2003-2014, Antonin Descampe
 2003-2007, Francois-Olivier Devaux
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002, 2003, Yannick Verschueren
 2001-2003, David Janssens
License: BSD-2-clause

Files: src/bin/jp2/index.c
 src/bin/jp2/index.h
Copyright: 2003-2007, Francois-Olivier Devaux
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
License: BSD-2-clause

Files: src/bin/jp2/opj_compress.c
Copyright: 2012, CS Systemes dInformation, France
 2011, 2012, Centre National dEtudes Spatiales (CNES), France
 2008, Jerome Fimes, Communications & Systemes <jerome.fimes@c-s.fr>
 2006, 2007, Parvatha Elangovan
 2005, Herve Drolon, FreeImage Team
 2003-2014, Antonin Descampe
 2003-2007, Francois-Olivier Devaux
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002, 2003, Yannick Verschueren
 2001-2003, David Janssens
License: BSD-2-clause

Files: src/bin/jp2/opj_decompress.c
Copyright: 2012, CS Systemes dInformation, France
 2008, 2011, 2012, Centre National dEtudes Spatiales (CNES), FR
 2006, 2007, Parvatha Elangovan
 2005, Herve Drolon, FreeImage Team
 2003-2014, Antonin Descampe
 2003-2007, Francois-Olivier Devaux
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002, 2003, Yannick Verschueren
 2001-2003, David Janssens
License: BSD-2-clause

Files: src/bin/jp2/opj_dump.c
Copyright: 2012, CS Systemes dInformation, France
 2011, 2012, Centre National dEtudes Spatiales (CNES), France
 2010, Mathieu Malaterre, GDCM
License: BSD-2-clause

Files: src/bin/jp2/windirent.h
Copyright: 1998-2002, Toni Ronkko
License: Expat

Files: src/bin/jpip/*
Copyright: 2010, 2011, Kaori Hagihara
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
License: BSD-2-clause

Files: src/bin/jpip/opj_server.c
Copyright: 2011, Lucian Corlaciu, GSoC
 2010, 2011, Kaori Hagihara
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
License: BSD-2-clause

Files: src/bin/wx/*
Copyright: no-info-found
License: BSD-2-clause

Files: src/bin/wx/OPJViewer/about/*
Copyright: 2005-2007, <a href="http://dsplab.diei.unipg.it/">DSPLab</a> - <a href="http://www.unipg.it/">Università degli studi di Perugia</a></font>
 2002-2007, <a href="http://www.tele.ucl.ac.be/">TELE</a> - <a href="http://www.uclouvain.be/">Université Catholique de Louvain</a></font>
License: BSD-2-clause

Files: src/bin/wx/OPJViewer/source/OPJAbout.cpp
 src/bin/wx/OPJViewer/source/OPJDialogs.cpp
 src/bin/wx/OPJViewer/source/OPJThreads.cpp
 src/bin/wx/OPJViewer/source/OPJViewer.cpp
 src/bin/wx/OPJViewer/source/OPJViewer.h
 src/bin/wx/OPJViewer/source/imagjpeg2000.h
 src/bin/wx/OPJViewer/source/imagmxf.h
Copyright: 2007, Digital Signal Processing Laboratory, Universita degli studi di Perugia (UPG), Italy
License: BSD-2-clause

Files: src/bin/wx/OPJViewer/source/about_htm.h
Copyright: 2007, 2008, <a href="http://dsplab.diei.unipg.it/">DSPLab</a> - <a href="http://www.unipg.it/">Universita degli studi di Perugia</a></font>"
 2002-2008, <a href="http://www.tele.ucl.ac.be/">TELE</a> - <a href="http://www.uclouvain.be/">Universite Catholique de Louvain</a></font>"
License: BSD-2-clause

Files: src/bin/wx/OPJViewer/source/imagjpeg2000.cpp
 src/bin/wx/OPJViewer/source/imagmxf.cpp
 src/bin/wx/OPJViewer/source/wxj2kparser.cpp
 src/bin/wx/OPJViewer/source/wxjp2parser.cpp
Copyright: 2007, Digital Signal Processing Laboratory, Università degli studi di Perugia (UPG), Italy
License: BSD-2-clause

Files: src/bin/wx/OPJViewer/source/license.txt
Copyright: 2007, Digital Signal Processing Laboratory, Università degli studi di Perugia (UPG), Italy
 2005, Herve Drolon, FreeImage Team
 2003-2007, Francois-Olivier Devaux and Antonin Descampe
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002, 2003, Yannick Verschueren
 2001-2003, David Janssens
License: BSD-2-clause

Files: src/lib/*
Copyright: 2010, 2011, Kaori Hagihara
 2003, 2004, Yannick Verschueren
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
License: BSD-2-clause

Files: src/lib/openjp2/bench_dwt.c
 src/lib/openjp2/sparse_array.c
 src/lib/openjp2/sparse_array.h
 src/lib/openjp2/test_sparse_array.c
Copyright: 2017, IntoPix SA <contact@intopix.com>
License: BSD-2-clause

Files: src/lib/openjp2/bio.c
 src/lib/openjp2/bio.h
 src/lib/openjp2/dwt.h
 src/lib/openjp2/opj_intmath.h
 src/lib/openjp2/pi.h
Copyright: 2005, Herve Drolon, FreeImage Team
 2003-2014, Antonin Descampe
 2003-2007, Francois-Olivier Devaux
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002, 2003, Yannick Verschueren
 2001-2003, David Janssens
License: BSD-2-clause

Files: src/lib/openjp2/cio.c
 src/lib/openjp2/cio.h
 src/lib/openjp2/mct.c
 src/lib/openjp2/mct.h
 src/lib/openjp2/tgt.c
Copyright: 2012, CS Systemes dInformation, France
 2008, 2011, 2012, Centre National dEtudes Spatiales (CNES), FR
 2005, Herve Drolon, FreeImage Team
 2003-2014, Antonin Descampe
 2003-2007, Francois-Olivier Devaux
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002, 2003, Yannick Verschueren
 2001-2003, David Janssens
License: BSD-2-clause

Files: src/lib/openjp2/dwt.c
Copyright: 2017, IntoPIX SA <support@intopix.com>
 2007, Jonathan Ballard <dzonatas@dzonux.net>
 2007, Callum Lerwick <seg@haxxed.com>
 2005, Herve Drolon, FreeImage Team
 2003-2014, Antonin Descampe
 2003-2007, Francois-Olivier Devaux
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002, 2003, Yannick Verschueren
 2001-2003, David Janssens
License: BSD-2-clause

Files: src/lib/openjp2/event.c
 src/lib/openjp2/event.h
 src/lib/openjp2/openjpeg.c
 src/lib/openjp2/opj_includes.h
Copyright: 2012, CS Systemes dInformation, France
 2008, 2011, 2012, Centre National dEtudes Spatiales (CNES), FR
 2005, Herve Drolon, FreeImage Team
License: BSD-2-clause

Files: src/lib/openjp2/function_list.c
 src/lib/openjp2/function_list.h
 src/lib/openjp2/invert.c
 src/lib/openjp2/invert.h
Copyright: 2008, Jerome Fimes, Communications & Systemes <jerome.fimes@c-s.fr>
License: BSD-2-clause

Files: src/lib/openjp2/ht_dec.c
 src/lib/openjp2/t1_ht_generate_luts.c
Copyright: 2021, The University of New South Wales, Australia
 2021, Kakadu Software Pty Ltd, Australia
 2021, Aous Naman
License: BSD-2-clause

Files: src/lib/openjp2/image.c
 src/lib/openjp2/image.h
 src/lib/openjp2/opj_clock.c
 src/lib/openjp2/opj_clock.h
Copyright: 2005, Herve Drolon, FreeImage Team
License: BSD-2-clause

Files: src/lib/openjp2/j2k.c
Copyright: 2017, IntoPIX SA <support@intopix.com>
 2012, CS Systemes dInformation, France
 2011, 2012, Centre National dEtudes Spatiales (CNES), France
 2010, 2011, Kaori Hagihara
 2008, Jerome Fimes, Communications & Systemes <jerome.fimes@c-s.fr>
 2006, 2007, Parvatha Elangovan
 2005, Herve Drolon, FreeImage Team
 2003-2014, Antonin Descampe
 2003-2007, Francois-Olivier Devaux
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002, 2003, Yannick Verschueren
 2001-2003, David Janssens
License: BSD-2-clause

Files: src/lib/openjp2/j2k.h
Copyright: 2012, CS Systemes dInformation, France
 2011, 2012, Centre National dEtudes Spatiales (CNES), France
 2008, Jerome Fimes, Communications & Systemes <jerome.fimes@c-s.fr>
 2006, 2007, Parvatha Elangovan
 2005, Herve Drolon, FreeImage Team
 2003-2014, Antonin Descampe
 2003-2007, Francois-Olivier Devaux
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002, 2003, Yannick Verschueren
 2001-2003, David Janssens
License: BSD-2-clause

Files: src/lib/openjp2/jp2.c
Copyright: 2012, CS Systemes dInformation, France
 2010, 2011, Kaori Hagihara
 2008, 2011, 2012, Centre National dEtudes Spatiales (CNES), FR
 2005, Herve Drolon, FreeImage Team
 2003-2014, Antonin Descampe
 2003-2007, Francois-Olivier Devaux
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002, 2003, Yannick Verschueren
 2001-2003, David Janssens
License: BSD-2-clause

Files: src/lib/openjp2/jp2.h
Copyright: 2012, CS Systemes dInformation, France
 2008, 2011, 2012, Centre National dEtudes Spatiales (CNES), FR
 2005, Herve Drolon, FreeImage Team
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002, 2003, Yannick Verschueren
License: BSD-2-clause

Files: src/lib/openjp2/mqc.c
 src/lib/openjp2/mqc.h
 src/lib/openjp2/mqc_inl.h
Copyright: 2008, Jerome Fimes, Communications & Systemes <jerome.fimes@c-s.fr>
 2005, Herve Drolon, FreeImage Team
 2003-2014, Antonin Descampe
 2003-2007, Francois-Olivier Devaux
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002, 2003, Yannick Verschueren
 2001-2003, David Janssens
License: BSD-2-clause

Files: src/lib/openjp2/openjpeg.h
Copyright: 2012, CS Systemes dInformation, France
 2011, 2012, Centre National dEtudes Spatiales (CNES), France
 2010, 2011, Kaori Hagihara
 2008, Jerome Fimes, Communications & Systemes <jerome.fimes@c-s.fr>
 2006, 2007, Parvatha Elangovan
 2005, Herve Drolon, FreeImage Team
 2003-2014, Antonin Descampe
 2003-2007, Francois-Olivier Devaux
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002, 2003, Yannick Verschueren
 2001-2003, David Janssens
License: BSD-2-clause

Files: src/lib/openjp2/opj_codec.h
Copyright: 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
License: BSD-2-clause

Files: src/lib/openjp2/opj_common.h
Copyright: 2017, IntoPIX SA <support@intopix.com>
License: BSD-2-clause

Files: src/lib/openjp2/opj_inttypes.h
 src/lib/openjp2/opj_stdint.h
Copyright: 2012, Mathieu Malaterre <mathieu.malaterre@gmail.com>
License: BSD-2-clause

Files: src/lib/openjp2/opj_malloc.c
Copyright: 2015, Matthieu Darbois
 2015, Mathieu Malaterre <mathieu.malaterre@gmail.com>
License: BSD-2-clause

Files: src/lib/openjp2/opj_malloc.h
Copyright: 2007, Callum Lerwick <seg@haxxed.com>
 2005, Herve Drolon, FreeImage Team
License: BSD-2-clause

Files: src/lib/openjp2/pi.c
Copyright: 2006, 2007, Parvatha Elangovan
 2005, Herve Drolon, FreeImage Team
 2003-2014, Antonin Descampe
 2003-2007, Francois-Olivier Devaux
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002, 2003, Yannick Verschueren
 2001-2003, David Janssens
License: BSD-2-clause

Files: src/lib/openjp2/t1.c
Copyright: 2017, IntoPIX SA <support@intopix.com>
 2012, Carl Hetherington
 2007, Callum Lerwick <seg@haxxed.com>
 2005, Herve Drolon, FreeImage Team
 2003-2014, Antonin Descampe
 2003-2007, Francois-Olivier Devaux
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002, 2003, Yannick Verschueren
 2001-2003, David Janssens
License: BSD-2-clause

Files: src/lib/openjp2/t1.h
Copyright: 2017, IntoPIX SA <support@intopix.com>
 2012, Carl Hetherington
 2005, Herve Drolon, FreeImage Team
 2003-2014, Antonin Descampe
 2003-2007, Francois-Olivier Devaux
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002, 2003, Yannick Verschueren
 2001-2003, David Janssens
License: BSD-2-clause

Files: src/lib/openjp2/t1_generate_luts.c
Copyright: 2012, Carl Hetherington
 2007, Callum Lerwick <seg@haxxed.com>
 2005, Herve Drolon, FreeImage Team
 2003-2014, Antonin Descampe
 2003-2007, Francois-Olivier Devaux
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002, 2003, Yannick Verschueren
 2001-2003, David Janssens
License: BSD-2-clause

Files: src/lib/openjp2/t2.c
 src/lib/openjp2/t2.h
 src/lib/openjp2/tcd.h
Copyright: 2017, IntoPIX SA <support@intopix.com>
 2012, CS Systemes dInformation, France
 2008, 2011, 2012, Centre National dEtudes Spatiales (CNES), FR
 2005, Herve Drolon, FreeImage Team
 2003-2014, Antonin Descampe
 2003-2007, Francois-Olivier Devaux
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002, 2003, Yannick Verschueren
 2001-2003, David Janssens
License: BSD-2-clause

Files: src/lib/openjp2/tcd.c
Copyright: 2017, IntoPIX SA <support@intopix.com>
 2012, CS Systemes dInformation, France
 2008, 2011, 2012, Centre National dEtudes Spatiales (CNES), FR
 2006, 2007, Parvatha Elangovan
 2005, Herve Drolon, FreeImage Team
 2003-2014, Antonin Descampe
 2003-2007, Francois-Olivier Devaux
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002, 2003, Yannick Verschueren
 2001-2003, David Janssens
License: BSD-2-clause

Files: src/lib/openjp2/tgt.h
Copyright: 2012, CS Systemes dInformation, France
 2011, 2012, Centre National dEtudes Spatiales (CNES), France
 2008, Jerome Fimes, Communications & Systemes <jerome.fimes@c-s.fr>
 2005, Herve Drolon, FreeImage Team
 2003-2014, Antonin Descampe
 2003-2007, Francois-Olivier Devaux
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002, 2003, Yannick Verschueren
 2001-2003, David Janssens
License: BSD-2-clause

Files: src/lib/openjp2/thread.c
 src/lib/openjp2/thread.h
 src/lib/openjp2/tls_keys.h
Copyright: 2016, Even Rouault
License: BSD-2-clause

Files: src/lib/openjpip/*
Copyright: 2010, 2011, Kaori Hagihara
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
License: BSD-2-clause

Files: src/lib/openjpip/dec_clientmsg_handler.h
 src/lib/openjpip/j2kheader_manager.c
 src/lib/openjpip/j2kheader_manager.h
 src/lib/openjpip/jp2k_encoder.c
 src/lib/openjpip/jp2k_encoder.h
 src/lib/openjpip/jpip_parser.c
 src/lib/openjpip/jpip_parser.h
 src/lib/openjpip/msgqueue_manager.c
 src/lib/openjpip/msgqueue_manager.h
 src/lib/openjpip/query_parser.c
 src/lib/openjpip/query_parser.h
Copyright: 2011, Lucian Corlaciu, GSoC
 2010, 2011, Kaori Hagihara
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
License: BSD-2-clause

Files: tests/*
Copyright: 2011, 2012, Centre National dEtudes Spatiales (CNES), France
License: BSD-2-clause

Files: tests/fuzzers/*
Copyright: 2017, IntoPix SA <contact@intopix.com>
License: BSD-2-clause

Files: tests/j2k_random_tile_access.c
Copyright: 2012, CS Systemes dInformation, France
 2011, 2012, Centre National dEtudes Spatiales (CNES), France
License: BSD-2-clause

Files: tests/nonregression/*
Copyright: no-info-found
License: BSD-2-clause

Files: tests/nonregression/checkmd5refs.cmake
Copyright: 2006-2014, Mathieu Malaterre <mathieu.malaterre@voxxl.com>
License: BSD-3-clause

Files: tests/pdf2jp2.c
 tests/ppm2rgb3.c
Copyright: 2014, Mathieu Malaterre <mathieu.malaterre@voxxl.com>
License: BSD-2-clause

Files: tests/performance/*
Copyright: 2017, IntoPIX SA
License: BSD-2-clause

Files: tests/profiling/*
Copyright: 2017, IntoPIX SA
License: BSD-2-clause

Files: tests/test_decode_area.c
Copyright: 2017, IntoPix SA <contact@intopix.com>
License: BSD-2-clause

Files: tests/test_tile_decoder.c
 tests/test_tile_encoder.c
Copyright: 2008, Jerome Fimes, Communications & Systemes <jerome.fimes@c-s.fr>
License: BSD-2-clause

Files: tests/unit/*
Copyright: 2010, 2012, Mathieu Malaterre
License: BSD-2-clause

Files: thirdparty/*
Copyright: 1998-2016, Marti Maria Saguer
License: Expat

Files: thirdparty/include/*
Copyright: 1995-2013, Jean-loup Gailly.
License: ZLIB

Files: thirdparty/include/zlib.h
Copyright: 1995-2013, Jean-loup Gailly and Mark Adler
License: Zlib

Files: thirdparty/libpng/*
Copyright: 2000-2002, 2004, 2006-2016, Glenn Randers-Pehrson, are
License: Libpng

Files: thirdparty/libpng/png.c
 thirdparty/libpng/png.h
 thirdparty/libpng/pngconf.h
 thirdparty/libpng/pngdebug.h
 thirdparty/libpng/pngerror.c
 thirdparty/libpng/pngget.c
 thirdparty/libpng/pnginfo.h
 thirdparty/libpng/pngmem.c
 thirdparty/libpng/pngpread.c
 thirdparty/libpng/pngpriv.h
 thirdparty/libpng/pngread.c
 thirdparty/libpng/pngrio.c
 thirdparty/libpng/pngrtran.c
 thirdparty/libpng/pngrutil.c
 thirdparty/libpng/pngset.c
 thirdparty/libpng/pngstruct.h
 thirdparty/libpng/pngtrans.c
 thirdparty/libpng/pngwio.c
 thirdparty/libpng/pngwrite.c
 thirdparty/libpng/pngwtran.c
 thirdparty/libpng/pngwutil.c
Copyright: 1998-2016, Glenn Randers-Pehrson
 1996, 1997, Andreas Dilger)
 1995, 1996, Guy Eric Schalnat, Group 42, Inc.)
License: Libpng

Files: thirdparty/libpng/pnglibconf.h
Copyright: 1998-2015, Glenn Randers-Pehrson
License: Libpng

Files: thirdparty/libtiff/*
Copyright: 1991-1997, Silicon Graphics, Inc.
 1988-1997, Sam Leffler
License: libtiff

Files: thirdparty/libtiff/libport.h
Copyright: 2009, Frank Warmerdam
License: libtiff

Files: thirdparty/libtiff/tif_luv.c
Copyright: 1997, Silicon Graphics, Inc.
 1997, Greg Ward Larson
License: libtiff

Files: thirdparty/libtiff/tif_lzma.c
Copyright: 2010, Andrey Kiselev <dron@ak4719.spb.edu>
License: libtiff

Files: thirdparty/libtiff/tif_pixarlog.c
Copyright: 1996, Pixar
 1996, 1997, Sam Leffler
License: libtiff

Files: thirdparty/libz/*
Copyright: 1995-2013, Mark Adler
License: ZLIB

Files: thirdparty/libz/CMakeLists.txt
 thirdparty/libz/crc32.h
 thirdparty/libz/inffixed.h
 thirdparty/libz/trees.h
Copyright: no-info-found
License: ZLIB

Files: thirdparty/libz/compress.c
 thirdparty/libz/uncompr.c
 thirdparty/libz/zutil.c
 thirdparty/libz/zutil.h
Copyright: 1995-2013, Jean-loup Gailly.
License: ZLIB

Files: thirdparty/libz/deflate.c
Copyright: 1995-2013, Jean-loup Gailly and Mark Adler
License: ZLIB

Files: thirdparty/libz/deflate.h
 thirdparty/libz/trees.c
Copyright: 1995-2012, Jean-loup Gailly
License: ZLIB

Files: thirdparty/libz/zlib.h
Copyright: 1995-2013, Jean-loup Gailly and Mark Adler
License: Zlib

Files: tools/*
Copyright: no-info-found
License: BSD-2-clause

Files: tools/ctest_scripts/*
Copyright: 2006-2014, Mathieu Malaterre <mathieu.malaterre@voxxl.com>
License: BSD-3-clause

Files: tools/ctest_scripts/travis-ci.cmake
Copyright: no-info-found
License: BSD-2-clause

Files: tools/travis-ci/detect-avx2.c
Copyright: 2017, IntoPIX SA <support@intopix.com>
License: BSD-2-clause

Files: tools/travis-ci/travis_rsa.enc
Copyright: no-info-found
License: BSD-2-clause

Files: wrapping/*
Copyright: 2007, Patrick Piscaglia (Telemis)
 2006, 2007, Parvatha Elangovan
 2005, Herve Drolon, FreeImage Team
 2003-2014, Antonin Descampe
 2003-2007, Francois-Olivier Devaux
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002, 2003, Yannick Verschueren
 2001-2003, David Janssens
License: BSD-2-clause

Files: wrapping/java/openjp2/index.c
 wrapping/java/openjp2/index.h
Copyright: 2003-2007, Francois-Olivier Devaux
 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
License: BSD-2-clause

Files: wrapping/java/openjp2/java-sources/*
Copyright: 2002-2014, Universite catholique de Louvain (UCL), Belgium
 2002-2014, Professor Benoit Macq
 2002-2007, Patrick Piscaglia, Telemis s.a.
License: BSD-2-clause

Files: thirdparty/libtiff/tif_ojpeg.c
Copyright: Joris Van Damme <info@awaresystems.be>
 AWare Systems <http://www.awaresystems.be/>
License: LIBTIFF

Files: thirdparty/libtiff/tiffvers.h
Copyright: 1988-1997 Sam Leffler
 1991-1997 Silicon Graphics, Inc.
License: LIBTIFF
